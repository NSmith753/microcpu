

public class Instruction
{
	private String type = "";
	private String registerID = "";
	private int value = 0;
	
	public Instruction()
	{
		this.type = "";
		this.registerID = "";
		this.value = 0;
	}
	
	public Instruction(String type, String registerID, int value)
	{
		this.type = type;
		this.registerID = registerID;
		this.value = value;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void setRegisterID(String registerID)
	{
		this.registerID = registerID;
	}
	
	public void setValue(int value)
	{
		this.value = value;
	}
	
	
	public String getType()
	{
		return this.type;
	}
	
	public String getRegisterID()
	{
		return this.registerID;
	}
	
	public int getValue()
	{
		return this.value;
	}
		
}
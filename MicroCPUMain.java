

public class MicroCPUMain
{
	public static void main(String[] args)
	{
		MicroCPU microCPU = new MicroCPU();
		
		microCPU.buildRegisterList();
		microCPU.process();
				
	}	
	 
}
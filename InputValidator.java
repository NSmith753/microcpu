import java.math.BigDecimal;
import javax.swing.*;


/**
 * @author		Nicholas Smith nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-08-22
 * 
 */
public class InputValidator
{
	private final String PERCENT_SIGN = "%"; // constant holding the string literal for the percent sign
	private final int FIRST_NUM = 0; // constant holding the array subscript location for the first number after the swap
	private final int SECOND_NUM = 1; // constant holding the array subscript location for the second number after the swap
	
	private final BigDecimal PERCENTAGE = new BigDecimal("100.00"); // constant used in the formula to convert a number into a percentage
	
	private HelperLib helperLib = new HelperLib();
	
	
	/**
	 * Validates the input data based on the specified data type.
	 *
	 * @param inputData the data that needs to be validated
	 * @param expectedDataType the data type the input data is expected to be
	 * @return isValid true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean validateInput(String inputData, String expectedDataType)
	{
		boolean isValid = true; // tracks the validation of the user input
		String userInput = ""; // holds the user input from the prompt
		
		expectedDataType = expectedDataType.toLowerCase();
		
		if (inputData == null)
		{
			System.out.println("The user cancelled the data entry process.");
			this.helperLib.displayMsg("Data entry process cancelled.", "Data Entry", JOptionPane.INFORMATION_MESSAGE);
			
			System.exit(0); // terminate program since continuing further would propagate errors throughout the system.
		}
		
		
		if (inputData == "")
		{
			this.helperLib.displayMsg("Invalid data entry, please enter a valid integer.", "Data Entry", JOptionPane.INFORMATION_MESSAGE);
			isValid = false;
		}
		
		switch (expectedDataType)
		{
			case "number":
				if (! isNumeric(inputData))
					isValid = false;
				
				break;
				
			case "alpha":
				if (! isAlpha(inputData))
					isValid = false;
				
				break;
				
			case "percentage":
				if (! isPercentage(inputData))
					isValid = false;
				
			case "alpha-numeric":
				break;
				
			default:
				isValid = false;
		}
		
		
		return isValid;		
	} // end of validateInput()
	
	
	/**
	 * Validates that the specified data is a numerical value.
	 *
	 * @param strValue the data that needs to be validated
	 * @return isNumeric true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean isNumeric(String strValue)
	{
		boolean isNumeric = true;
		
		try
		{
			Double.parseDouble(strValue); // try to convert to a double, if it throws an exception it is not numerical
		}
		catch(NumberFormatException e)
		{
			isNumeric = false;
		}
		
		return isNumeric;
	} // end of isNumeric()
	
	
	/**
	 * Validates that the specified data contains letters only.
	 *
	 * @param strValue the data that needs to be validated
	 * @return true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean isAlpha(String strValue)
	{	
		return strValue.chars().allMatch(ch -> Character.isLetter(ch)); // check all characters in the string are letters
	} // end of isAlpha()
	
	
	/**
	 * Validates that the specified data is a numerical value and is a percentage.
	 *
	 * @param strValue strValue the data that needs to be validated
	 * @return isPercentage true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean isPercentage(String strValue)
	{		
		boolean isPercentage = true;
		String[] strValueSplit;
		
		if (strValue.contains(this.PERCENT_SIGN))
		{
			strValueSplit = strValue.split(this.PERCENT_SIGN);
			
			if (strValueSplit.length == 1) // if there is a data before the percent sign
			{
				if (strValueSplit[this.FIRST_NUM].length() != 0) //  if there is a data before the percent sign
				{					
					if (! isNumeric(strValueSplit[this.FIRST_NUM])) // check to see if the data is numerical
					{
						isPercentage = false;
					}
				}
				else
				{
					isPercentage = false;
				}
			}
			else
			{
				isPercentage = false;
			}
		}
		else
		{
			isPercentage = false;
		}
			
		
		return isPercentage;		
	} // end of isPercentage()
	
	
	/**
	 * Removes the percent sign from the specified percentage.
	 *
	 * @param strValue the data that needs to have the percent '%' sign removed
	 * @return strValue without the trailing percent '%' sign
	 *
	 */ 
	public String removePercentSign(String percentage)
	{
		return percentage.split(this.PERCENT_SIGN)[this.FIRST_NUM];
	} // end of removePercentSign()
	
	
	/**
	 * Converts the specified number value to a percentage value without the percent '%' sign.
	 *
	 * @param value the data that needs to be converted
	 * @return value converted to a percentage form
	 *
	 */ 
	public String convertNumberToPercentage(String value)
	{
		BigDecimal valueBD = new BigDecimal(value);
		
		return String.valueOf(valueBD.multiply(PERCENTAGE).doubleValue());
	} // end of convertNumberToPercentage()
	
	
	/**
	 * Converts the specified percentage value to a number value
	 *
	 * @param percentage the data that needs to be converted
	 * @return percentage converted to a number form
	 *
	 */ 
	public String convertPercentageToNumber(String percentage)
	{
		BigDecimal percentageBD = new BigDecimal(percentage);
		
		return String.valueOf(percentageBD.divide(PERCENTAGE).doubleValue());
	} // end of convertPercentageToNumber()
}